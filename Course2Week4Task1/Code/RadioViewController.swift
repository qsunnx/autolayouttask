//
//  RadioViewController.swift
//  Course2Week4Task1
//
//  Copyright © 2018 e-Legion. All rights reserved.
//

import UIKit

class RadioViewController: UIViewController {
    
    private var albumImage: UIImageView!
    private var progressView: UIProgressView!
    private var timeSlider: UISlider!
    private var albumName: UILabel!
    private var compactRegularSizeConstraints = [NSLayoutConstraint]()
    private var compactCompactSizeConstraints = [NSLayoutConstraint]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createLocalViews()
        addLocalSubviewsToSuperView()
        setupConstraints()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if traitCollection.verticalSizeClass != previousTraitCollection?.verticalSizeClass {
            enableConstraintsForHeight(heightClass: traitCollection.verticalSizeClass)
        }
    }

    private func createLocalViews() {
        createAlbumImage()
        createProgressView()
        createAlbumNameLabel()
        createTimeSlider()
    }
    
    private func createAlbumImage() {
        albumImage = UIImageView()
        albumImage.translatesAutoresizingMaskIntoConstraints = false
        albumImage.contentMode                               = .scaleAspectFill
    }
    
    private func createProgressView() {
        progressView = UIProgressView()
        progressView.translatesAutoresizingMaskIntoConstraints = false
        progressView.progress                                  = 0.5
    }
    
    private func createAlbumNameLabel() {
        albumName = UILabel()
        albumName.translatesAutoresizingMaskIntoConstraints = false
        albumName.textAlignment                             = .center
        albumName.textColor                                 = UIColor.black
        albumName.numberOfLines                             = 1
        albumName.font                                      = UIFont.systemFont(ofSize: 22)
        albumName.text                                      = "Rammstein - Mutter"
    }
    
    private func createTimeSlider() {
        timeSlider = UISlider()
        timeSlider.translatesAutoresizingMaskIntoConstraints = false
        timeSlider.value                                     = 0.5
    }
    
    private func addLocalSubviewsToSuperView() {
        view.addSubview(albumImage)
        view.addSubview(progressView)
        view.addSubview(albumName)
        view.addSubview(timeSlider)
    }
    
    private func setupConstraints() {
        // Constant constraints
        let safeArea = view.safeAreaLayoutGuide
        // ImageView
        albumImage.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: 16.0).isActive                   = true
        albumImage.widthAnchor.constraint (equalTo: albumImage.heightAnchor, multiplier: 1.0/1.0, constant: 0).isActive = true
        
        // ProgressView
        progressView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: 16.0).isActive                 = true
        safeArea.trailingAnchor.constraint(equalTo: progressView.trailingAnchor, constant: 16.0).isActive               = true
        
        // Slider
        timeSlider.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: 16.0).isActive                   = true
        safeArea.trailingAnchor.constraint(equalTo: timeSlider.trailingAnchor, constant: 16.0).isActive                 = true
        safeArea.bottomAnchor.constraint(equalTo: timeSlider.bottomAnchor, constant: 24.0).isActive                     = true
        timeSlider.heightAnchor.constraint(equalToConstant: 31.0).isActive                                              = true
        
        // Constraints for defferent height
        // View
        compactRegularSizeConstraints.append(safeArea.trailingAnchor.constraint(equalTo: albumImage.trailingAnchor, constant: 16.0))
        compactRegularSizeConstraints.append(safeArea.trailingAnchor.constraint(equalTo: albumName.trailingAnchor, constant: 16.0))
        
        compactCompactSizeConstraints.append(safeArea.trailingAnchor.constraint(equalTo: albumName.trailingAnchor, constant: 16.0))
        
        // ImageView
        compactRegularSizeConstraints.append(albumImage.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: 8.0))
        compactRegularSizeConstraints.append(albumImage.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor))
        
        compactCompactSizeConstraints.append(albumImage.topAnchor.constraint(equalTo: progressView.bottomAnchor, constant: 16.0))
        
        // ProgressView
        compactRegularSizeConstraints.append(progressView.topAnchor.constraint(equalTo: albumImage.bottomAnchor, constant: 30.0))
        
        compactCompactSizeConstraints.append(progressView.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: 16.0))
        
        // Label
        compactRegularSizeConstraints.append(albumName.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: 16.0))
        compactRegularSizeConstraints.append(albumName.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor))
        
        compactCompactSizeConstraints.append(albumName.leadingAnchor.constraint(equalTo: albumImage.trailingAnchor, constant: 16.0))
        compactCompactSizeConstraints.append(albumName.topAnchor.constraint(equalTo: progressView.bottomAnchor, constant: 110.0))
        
        // Slider
        compactRegularSizeConstraints.append(timeSlider.topAnchor.constraint(equalTo: albumName.bottomAnchor, constant: 73.0))
        
        compactCompactSizeConstraints.append(timeSlider.topAnchor.constraint(equalTo: albumImage.bottomAnchor, constant: 16.0))
        
        enableConstraintsForHeight(heightClass: traitCollection.verticalSizeClass)
    }
    
    private func enableConstraintsForHeight(heightClass: UIUserInterfaceSizeClass) {
        if heightClass == .regular {
            NSLayoutConstraint.deactivate(compactCompactSizeConstraints)
            NSLayoutConstraint.activate(compactRegularSizeConstraints)
        } else {
            NSLayoutConstraint.deactivate(compactRegularSizeConstraints)
            NSLayoutConstraint.activate(compactCompactSizeConstraints)
        }
    }

    
}
